package com.company;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ErrorStage {
    Stage errorStage = new Stage();

    public void showErrorMessage(String errorString) {
        Label closeWindowLabel = new Label(errorString+" is not exist");
        closeWindowLabel.setWrapText(true);
        closeWindowLabel.setStyle("-fx-font-weight: 700");
        closeWindowLabel.setPadding(new Insets(50,0,50,0));

        Button closeWindowButton = new Button("OK");
        closeWindowButton.setPrefWidth(80);
        closeWindowButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                errorStage.close();
            }
        });

        VBox closeWindowVBox = new VBox(closeWindowLabel, closeWindowButton);
        closeWindowVBox.setAlignment(Pos.CENTER);
        closeWindowVBox.setPadding(new Insets(0,0,50,0));
        Scene scene = new Scene(closeWindowVBox);

        errorStage.setScene(scene);

        errorStage.setTitle("Error");
        errorStage.setWidth(250);
        errorStage.setHeight(200);

        errorStage.show();
    }
}
