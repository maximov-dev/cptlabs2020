package com.company;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

//Maximov
//TODO add some filters

public class Main extends Application {
    private String filePath = "";

    public void start(Stage stage) throws Exception{

        NumberAxis x = new NumberAxis();
        x.setLabel("Threads");
        NumberAxis y = new NumberAxis();
        y.setLabel("Time");
        LineChart<Number, Number> lineChart = new LineChart<Number, Number>(x,y);

        TextField threadCountInputBox = new TextField("");
        Button applyFilterButton = new Button("Apply Filter");

        final FileChooser fileChooser = new FileChooser();
        Button fileChooserButton = new Button("Select One File and Open");

        Stage newWindow = new Stage();

        fileChooserButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                File file = fileChooser.showOpenDialog(newWindow);
                if (file != null) {
                    filePath = file.getAbsolutePath();
                }
            }
        });

        applyFilterButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {

                if(filePath.equals("") || threadCountInputBox.getText().equals("")) {
                    ErrorStage errorStage = new ErrorStage();
                    errorStage.showErrorMessage("Threads count or file path");
                    return;
                }

                int threadCount = Integer.valueOf(threadCountInputBox.getText());
                XYChart.Series series = new XYChart.Series();
                series.setName(String.valueOf(threadCount));

                for(int i = 1;i <= threadCount;i++ ) {
                    ImageStore store = new ImageStore(filePath);
                    store.phaser.register();
                    int step = store.image.getHeight() / i;
                    for (int threadIndex = 0; threadIndex < i; threadIndex++) {
                        new Worker(store, threadIndex * step, (threadIndex + 1) * step).start();
                    }
                    long startTime = System.currentTimeMillis();
                    store.phaser.arriveAndAwaitAdvance();
                    store.phaser.arriveAndAwaitAdvance();
                    long endTime = System.currentTimeMillis();

                    store.saveResult();
                    XYChart.Data d = new XYChart.Data(Integer.valueOf(i), endTime - startTime);
                    series.getData().add(d);
                }
                lineChart.getData().add(series);
            }
        });
        HBox control = new HBox(threadCountInputBox, applyFilterButton, fileChooserButton);
        control.setAlignment(Pos.CENTER);
        VBox group = new VBox(lineChart,control);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Multi Thread Image Processing App");
        stage.setHeight(400);
        stage.setWidth(800);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
