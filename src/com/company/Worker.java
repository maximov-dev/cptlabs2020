package com.company;

import java.awt.*;

public class Worker extends  Thread {
    private ImageStore store;
    private int startLine;
    private int endLine;

    public Worker(ImageStore store, int startLine, int endLine)
    {
        this.store = store;
        this.startLine = startLine;
        this.endLine = endLine;
        store.phaser.register();
    }

    @Override
    public void run(){
        store.phaser.arriveAndAwaitAdvance();

        for(int y = startLine; y < endLine; y++){
            for(int x = 0; x < store.image.getWidth(); x++){
                Color color = new Color(store.image.getRGB(x, y));
                int grayScale = (int) ((color.getRed() * 0.299) + (color.getGreen() * 0.587) + (color.getBlue() * 0.114));
                Color newColor = new Color(2, grayScale, 3);
                store.image.setRGB(x, y, newColor.getRGB());
            }
        }
        store.phaser.arriveAndAwaitAdvance();
    }
}
